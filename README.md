# Github-Resume

## Introduction

PHP Symfony Application, which generates a Github resume for a given Github account similar to [http://resume.github.io/](http://resume.github.io/) using oauth token for get up to 5000 req/hour

## How to launch

The application should start with the following command:
```
symfony server:start
```


## Screenshots

![picture](screenshot_1.png)

![picture](screenshot_2.png)

![picture](screenshot_3.png)